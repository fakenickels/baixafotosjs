/*globals require*/
require.config({
    shim: {

    },
    paths: {
        famous: '../lib/famous',
        requirejs: '../lib/requirejs/require',
        almond: '../lib/almond/almond',
        'famous-polyfills': '../lib/famous-polyfills/index',
        jszip: '../lib/jszip/jszip.min',
        hello: '../lib/hello/dist/hello.all.min'
    }
});
require(['main']);
