hello.init({
	facebook: "1414700908805329"
});

/* globals define */
define(function(require, exports, module) {
	'use strict';
	// import dependencies
	var Engine = require('famous/core/Engine');
	var Surface = require('famous/core/Surface');
	var ImageSurface = require('famous/surfaces/ImageSurface'); 
	var Modifier = require('famous/core/Modifier');
	var ContainerSurface = require("famous/surfaces/ContainerSurface");
	var View = require("famous/core/View");
	var GridLayout = require('famous/views/GridLayout');
	var Scrollview = require("famous/views/Scrollview");
	var HeaderFooterLayout = require("famous/views/HeaderFooterLayout");

	var RenderController = require("famous/views/RenderController");
	
	// create the main context and define views

	var mainContext = Engine.createContext(),
		renderCtrl = new RenderController();

	var FrontView = {
		View: void 0,

		loginBtnContainer: void 0,

		loginBtn: void 0,	

		setIt: function(){
			// FrontView tree
			var view = new View();

			var HomeGrid = new GridLayout({
				dimensions: [1,2]
			});

			var btnContainer = new ContainerSurface({
				size: [void 0, void 0]
			});

			var logo = new ImageSurface({
				size: [void 0, void 0],
				content: '/content/images/bflogo.png',
				properties: {
					background: '#333'
				}
			});

			var loginBtn = new Surface({
				size: [200, 100],
				content: 'Entre com o Facebook',

				classes: ['grey-bg'],
				properties: {
					color: '#fff',
					textAlign: 'center',
					lineHeight: 100 + 'px'
				}
			});

			var modifier = new Modifier({
				origin: [0.5, 0.5]
			});

			loginBtn.on('click', function(){
				hello.login('facebook', {
					scope: 'photos'
				});
			});

			btnContainer
				.add( modifier )
				.add( loginBtn );

			HomeGrid.sequenceFrom([
				logo,
				btnContainer
			]);

			view._add(HomeGrid);

			FrontView.loginBtnContainer = btnContainer;			
			FrontView.loginBtn = loginBtn;
			FrontView.View = view;
		}
	}


	var DownloadingView = {
		View: void 0,
		surface: void 0,
		setIt: function(){
			var view = new ContainerSurface({
				size: [void 0, void 0]
			});

			var surface = new Surface({
				size: [void 0, void 0],
				content: 'Baixando e zipando',
				classes: ['grey-bg'],

				properties: {
					color: '#fff',
					textAlign: 'center',
					lineHeight: window.innerHeight + 'px'
				}
			});

			view.add(surface);

			DownloadingView.surface = surface;
			DownloadingView.View = view;
		}
	}

	var SelectAlbumView = {
		View: void 0,
		layout: void 0,
		headerNextButton: void 0,

		setIt: function(){
			// MainView tree
			var view = new View();

			var layout = new HeaderFooterLayout({
				headerSize: 100,
				footerSize: 0
			});

			layout.header.add(
				new Surface({
					size: [void 0, 100],
					content: 'Clique num album para baixar e zipar. Allons-y!',

					classes: ['grey-bg'],
					properties: {
						color: '#fff',
						textAlign: 'center',
						lineHeight: '100px'
					}					
				})				
			);	

			view._add(layout);

			SelectAlbumView.View = view;
			SelectAlbumView.layout = layout;			
		},

		runIt: function(){
			var albumsScroll = new Scrollview(),
				albumsArr = [];
			
			albumsScroll.sequenceFrom( albumsArr );
			hello('facebook').api('/me/albums', function(albums, pagging){
				console.log('Context: main.js:118');
				console.log(arguments);

				albums.data.forEach(function(album, i){
					var temp = new Surface({
						size: [void 0, 200],
						content: album.name,

						properties: {
							backgroundColor: "hsl(" + (i * 360 / 40) + ", 100%, 50%)",
							lineHeight: '200px'
						}
					});

					temp.on('click', function(){
						renderCtrl.show(DownloadingView.View);

						var photosData = [];

						hello('facebook').api('/' + album.id + '/photos', function(photos, paging){
							photosData = photosData.concat(photos.data);
							
							DownloadingView
								.surface
								.setContent( 'Preparando para baixar ' + photosData.length + ' imagen(s).' );

							if( paging == null ){
								DownloadingView
									.surface
									.setContent('Baixando e zipando as imagens. Yowzah!');

								BaixaFotos.app.downloadAndZip( photosData, function(counter, totalLng){ // step
									console.log('Dowloading #' + counter);
									var percent = Math.round( (counter+1)/totalLng * 100),
										msg = '';

									if( percent !== 100 ) msg = percent + '% baixado(s)';
									else msg = "Zipando, aguarde...";

									DownloadingView.surface.setContent( msg );

								}, function(){ // callback
								 	renderCtrl.show( SelectAlbumView.View );
								});
							} else paging();
						})
					});

					temp.pipe(albumsScroll);
					albumsArr.push(temp);				
				});

				SelectAlbumView.layout.content.add(albumsScroll);
				renderCtrl.show( SelectAlbumView.View );
			});	
		} 
	}

	var SelectAlbumsPhotos = {
		setIt: function(){
			var imageGrid = new GridLayout({
				dimensions: [5, 5]
			});
		},

		runIt: function(){

		}
	}


	var BaixaFotos = {
		init: function(){
			FrontView.setIt();
			DownloadingView.setIt();
			SelectAlbumView.setIt();

			mainContext.add(renderCtrl);
			renderCtrl.show(FrontView.View);
		},

		SelectAlbumView: function(){
			FrontView.loginBtn.setContent('Carregando...');
			SelectAlbumView.runIt();
		},

		app: {
			// https://gist.github.com/HaNdTriX/7704632
			imgToDataURL: function(url, callback, outputFormat, quality) {
				var canvas = document.createElement('CANVAS'),
					ctx = canvas.getContext('2d'),
					img = new Image();
				img.crossOrigin = 'Anonymous';
				img.onload = function() {
					var dataURL;
					canvas.height = img.height;
					canvas.width = img.width;
					try {
						ctx.drawImage(img, 0, 0);
						dataURL = canvas.toDataURL(outputFormat, quality);
						callback(null, dataURL);
					} catch (e) {
						callback(e, null);
					}
					canvas = img = null;
				};
				img.onerror = function() {
					callback(new Error('Could not load image'), null);
				};
				img.src = url;
			},

			downloadAndZip: function( images, step, callback ){
				var zip = new JSZip(), counter = 0, totalLng = images.length;

				images.forEach(function(image, i){
					BaixaFotos.app.imgToDataURL( image.source, function(e, data){
						step(counter, totalLng);

						if(!e){
							zip.file( image.id + ".jpg", data.split(',')[1], {base64: true} );
							if( counter === images.length-1 ){
								if( callback ){
									callback();
								}

								var content = zip.generate({type:"blob"});
								saveAs(content, "BaixaFotosDownload.zip");
				   			} else { 
				   				counter++; 
				   			}
				  		} else { 
				  			console.log('hory shito', e) 
				  		}

				  	}, 'image/jpeg');
			 	});
			}
		}
	}

	BaixaFotos.init();

	window.SelectAlbumView = SelectAlbumView;

	hello.on('auth.login', BaixaFotos.SelectAlbumView);

	hello.off('auth.login', function(){
		
	});	
});
